'use strict';

const UrlHelper = {

    getDomainUrl: (account, service) => {
        return "http://api.liveperson.net/api/account/"+account+"/service/"+service+"/baseURI.json?version=1.0";   
    },

    getLoginUrl: (domain, account) => {
       return  "https://"+domain+"/api/account/"+account+"/login?v=1.3";
    },

    getAgentActivity: (domain, account) =>{

        return "https://"+domain+"/operations/api/account/"+account+"/agentactivity?timeframe=1440&agentIds=all&v=1";
    },

    getAgentStatus: (domain, account) =>{

        return "https://"+domain+"/messaging_history/api/account/"+account+"/agent-view/summary";
    },

    getQueueHealth: (domain, account) =>{

       return "https://"+domain+"/operations/api/account/"+account+"/queuehealth?timeframe=30&skillIds=all&interval=20&v=1";
    },

    getCurrentQueueHealth: (domain, account) =>{

        return "https://"+domain+"/operations/api/account/"+account+"/msgqueuehealth/current/?skillIds=&v=1";
     },
    
     getRefreshToken: (domain, account) =>{ 
         return "https://"+domain+"/api/account/"+account+"/refresh"
     }
}


module.exports = {
    UrlHelper
}