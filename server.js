'use strict';

const express = require('express');
const axios = require('axios');
const cron = require('node-cron');
const app = express();
const uHelper = require('./helpers/UrlHelper').UrlHelper;
const config = require('./resources/config');
const User = require('./model/User').User

/**
 *  getting login domain
 */
axios.get(uHelper.getDomainUrl(config.account, config.agentVep))
     .then( response =>{

        /* logs here */
        let domain = response.data.baseURI;
 
        /**
         *  getting bearer token
         */
        axios.post(uHelper.getLoginUrl(domain, config.account), new User(config.userCredentials))
             .then( response => {

                /* logs here */
                const token = 'Bearer '+response.data.bearer;
          
                
                /**
                 *  Getting data reporting domain
                 */
                axios.get(uHelper.getDomainUrl(config.account, config.leDataReporting))
                     .then( response => {

                        /* logs here */
                        let domain = response.data.baseURI;

                        /**
                         *  Starting Cron
                         */
                         cron.schedule('0 */1 * * * *', () => {
 
                            /* logs here */
                            console.log(new Date());

                            /**
                             *  Getting Current Queue Health
                             */
                           
                            axios.get(uHelper.getCurrentQueueHealth(domain, config.account),{headers: {"Authorization": config.Oauth}})
                                 .then(response =>{

                                    /* queue health logs here */
                                    console.log(response.data);

                                    /**
                                     * Getting Agent Status Domain
                                     */
                                    axios.get(uHelper.getDomainUrl(config.account, config.msgHist))
                                         .then( response => {

                                            /* logs here */
                                            let domain = response.data.baseURI;

                                            /**
                                             * Getting current agent status
                                             */
                                            axios.post(uHelper.getAgentStatus(domain, config.account),{},{headers: {"Authorization":token}})
                                                 .then( response => {

                                                    /* logs here */
                                                    console.log(response.data.summaryResults);
                                            
                                                 })
                                                 .catch( error =>{

                                                    /* logs here */
                                                    console.log(error);

                                                 });

                                         })
                                         .catch( error => {

                                            /* logs here */
                                            console.log(error);
                                         });
                                 })
                                 .catch( error =>{

                                    /* logs here */
                                    console.log(error);

                                 });

                        });
                        
                     }) 
                     .catch( error => {

                        /* logs here */
                        console.log(error);
                     });

               

             })
             .catch( error =>{

                /* logs here */
                console.log(error)
             });

     })
     .catch( error => {

        /* logs here */
        console.log(error)
     });

     
/**
 * 
 */
app.listen(3000, (err) =>{

    if(err){
        console.log("error starting server at port 3000");
    }else{
        console.log("server listening at port 3000");
    }
})

